<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttendanceSummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }
    
    public function index(){
        $student = Student::with(['attendance'])->where('students.section_id', '=', Request()->section_id)
                ->where('students.instructor_id', '=', Auth::id())
                ->where('students.semester_id', '=', Request()->semester_id)
                ->where('students.school_year_id', '=', Request()->school_year_id)->get();
        return response()->json($student);
    }
}
