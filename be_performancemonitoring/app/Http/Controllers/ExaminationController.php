<?php

namespace App\Http\Controllers;

use App\Models\Examination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExaminationController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }
    
    public function index(){
        $exam = DB::table('students')
                ->where('students.section_id', '=', Request()->section_id)
                ->where('students.instructor_id', '=', Auth::id())
                ->where('students.semester_id', '=', Request()->semester_id)
                ->where('students.school_year_id', '=', Request()->school_year_id)
                ->leftJoin('examinations', function ($join){
                    $join->on('students.id', '=', 'examinations.student_id')
                     ->where('examinations.semester_id', '=', Request()->semester_id)
                     ->where('examinations.examination_type_id', '=', Request()->examination_type_id)
                     ->where('examinations.school_year_id', '=', Request()->school_year_id);
                })->select('students.*', 'examinations.score', 'examinations.total')
                ->orderBy('students.gender', 'desc')
                ->orderBy('students.last_name')
                ->orderBy('students.first_name')->get();

        return response()->json($exam);
    }

    public function store(Request $request){
        $exam = Examination::where('semester_id', $request->semester_id)
            ->where('school_year_id', $request->school_year_id)
            ->where('student_id', $request->student_id)
            ->where('examination_type_id', $request->examination_type_id)
            ->first();

        if(empty($exam)){
            Examination::create($request->all());
        }
        else {
            $exam->update(['score' => $request->score, 'total' => $request->total]);
        }
    }

    public function update(){
        //
    }
}
