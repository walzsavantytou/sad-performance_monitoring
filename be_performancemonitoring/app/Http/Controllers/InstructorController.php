<?php

namespace App\Http\Controllers;

use App\Models\Instructor;
use Illuminate\Http\Request;

class InstructorController extends Controller
{
    public function store(Request $request){
        $instructor = Instructor::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'component_id' => $request->component_id
            ]);
            
        return response()->json($instructor, 200);
    }
}
