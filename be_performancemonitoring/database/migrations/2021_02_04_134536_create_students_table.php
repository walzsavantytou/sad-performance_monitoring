<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary()->unique();
            $table->string('first_name', 40);
            $table->string('middle_name', 40);
            $table->string('last_name', 40);
            $table->string('gender', 20);
            $table->foreignId('instructor_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('section_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('component_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('semester_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('school_year_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
