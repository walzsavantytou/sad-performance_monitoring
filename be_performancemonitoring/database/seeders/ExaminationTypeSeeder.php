<?php

namespace Database\Seeders;

use App\Models\ExaminationType;
use Illuminate\Database\Seeder;

class ExaminationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $examtype = [
            ['examination_type' => 'Pre Test'],
            ['examination_type' => 'Midterm Examination'],
            ['examination_type' => 'Finals Examination'],
        ];

        foreach ($examtype as $e) {
            ExaminationType::create($e);
        }
    }
}
