<?php

namespace Database\Seeders;

use App\Models\Semester;
use Illuminate\Database\Seeder;

class SemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $semester = [
            ['semester' => '1st Semester'],
            ['semester' => '2nd Semester'],
        ];

        foreach ($semester as $s) {
            Semester::create($s);
        }
    }
}
