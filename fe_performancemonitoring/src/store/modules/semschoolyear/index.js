import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const semester = 'semester'
const schoolyr = 'schoolyear'

export default ({
  namespaced: true,
  state: {
    schoolyr: [],
    semester: [],
    defaults: {
      semester_id: '',
      section_id: '',
      school_year_id: '',
    }
  },
  actions: {
    async getSemester({commit}){
      const res = await AXIOS.get(`${semester}`).then(response => {
        commit('SET_SEMESTER', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async getSchoolYear({commit}){
      const res = await AXIOS.get(`${schoolyr}`).then(response => {
        commit('SET_SCHOOLYR', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
  },
  getters: {
   getSemester(state){
     return state.semester
   },
   getDefaults(state){
     return state.defaults
   }
  },
  mutations: {
    SET_SEMESTER(state, data){
      state.semester = data
    },
    SET_SCHOOLYR(state, data){
      state.schoolyr = data
    },
    SET_DEFAULTS(state, data){
      state.defaults = data
    }
  },
})