import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const summary = 'overallsummary'

export default ({
  namespaced: true,
  state: {
    overallsummary: [],
  },
  actions: {
    async getOverAllSummary({commit}, data){
      const res = await AXIOS.get(`${summary}?section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        commit('SET_OVERALL_SUMMARY', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
  },
  getters: {
    getExamSummary(state){
      return state.examsummary;
    },
  },
  mutations: {
    SET_OVERALL_SUMMARY(state, data){
      for(let i = 0; i < data.length; i++){
        data[i].pretest = 0
        data[i].pretesttotal = 0
        data[i].midterm = 0
        data[i].midtermtotal = 0
        data[i].finals = 0
        data[i].finalstotal = 0
        for(let j = 0; j < data[i]['examination'].length; j++){
          if(data[i]['examination'][j]['examination_type_id'] == 1){
            data[i].pretest = data[i]['examination'][j]['score'];
            data[i].pretesttotal = data[i]['examination'][j]['total'];
          }
          else if(data[i]['examination'][j]['examination_type_id'] == 2){
            data[i].midterm = data[i]['examination'][j]['score'];
            data[i].midtermtotal = data[i]['examination'][j]['total'];
          }
          else if(data[i]['examination'][j]['examination_type_id'] == 3){
            data[i].finals = data[i]['examination'][j]['score'];
            data[i].finalstotal = data[i]['examination'][j]['total'];
          }
        }
      }

      let meetings = 0;
      for(let i = 0; i < data.length; i++){
        data[i].presentdays = 0
        if(data[i]['attendance'].length > meetings){
          meetings = data[i]['attendance'].length;
        }
        for(let j = 0; j < data[i]['attendance'].length; j++){
          if(data[i]['attendance'][j]['status'] == 'Present'){
            data[i].presentdays++;
          }
        }
      }

      //SET TOTAL MEETINGS
      for(let m = 0; m < data.length; m++){
        data[m].totalmeetings = meetings;
        if(data[m]['merit'] == null){
          data[m]['merit'] = 0;
        }
      }

      
      state.overallsummary = data
    },
  },
})