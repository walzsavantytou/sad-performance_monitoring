import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const examtype = 'examinationtype'
const exam = 'examination'

export default ({
  namespaced: true,
  state: {
    examtypes: [],
    examrecord: [],
  },
  actions: {
    async getExaminationRecord({commit}, data){
      const res = await AXIOS.get(`${exam}?examination_type_id=${data.examination_type_id}&section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        commit('SET_EXAM_RECORD', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async updateExaminationScore({commit}, data){
      const res = await AXIOS.post(`${exam}?student_id=${data.student_id}&score=${data.score}&total=${data.total}&examination_type_id=${data.examination_type_id}&section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;  
    },
    async getExaminationTypes({commit}){
      const res = await AXIOS.get(`${examtype}`).then(response => {
        commit('SET_EXAMTYPES', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
  },
  getters: {
   getComponent(state){
     return state.data
   },
  },
  mutations: {
    SET_EXAMTYPES(state, data){
      state.examtypes = data
    },
    SET_EXAM_RECORD(state, data){
      state.examrecord = data
    },
  },
})