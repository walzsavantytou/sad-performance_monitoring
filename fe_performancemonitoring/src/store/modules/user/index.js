import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const AUTH = 'auth'
const ACC = 'account'
export default ({
  namespaced: true,
  state: {
  },
  actions: {
    async signUpUser({commit}, data){
      const res = await AXIOS.post(`${AUTH}/store`, data).then(response => {
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async createInstructor({commit}, data){
      const res = await AXIOS.post(`${ACC}`, data).then(response =>{
        return response;
      }).catch(error => {
        return error.response
      });

      return res;
    },
  },
  getters: {
  },
  mutations: {
  },
})