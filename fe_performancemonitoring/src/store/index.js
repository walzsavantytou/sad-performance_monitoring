import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);

import auth from "./modules/authentication";
import user from "./modules/user";
import student from "./modules/student";
import component from "./modules/component";
import attendance from "./modules/attendance";
import exam from "./modules/examination";
import sem from "./modules/semschoolyear";
import aptitude from "./modules/aptitude";
import overallsummary from "./modules/overallsummary";
import summary from "./modules/attendance/summary";
import examsummary from "./modules/examination/summary";

export default new Vuex.Store({
  modules: {
    sem,
    user,
    auth,
    exam,
    summary,
    student,
    aptitude,
    component,
    attendance,
    overallsummary,
    examsummary,
  }
})